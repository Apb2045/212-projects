/*****************************************************
implementation file for the hash table class.
*****************************************************/

#include "hashtbl.h"
#include <iostream>
using std::endl;
using std::ostream;
#include <string.h> /* for strcpy */
#include <algorithm> /* for find */
using std::find;
#include <iomanip>
using std::setfill;
using std::setw;
#include <cassert>

#define R32 (rand()*100003 + rand())
#define R64 (((uint64_t)R32 << 32) + R32)
#define TLEN ((size_t)(1 << this->nBits))

namespace csc212
{
	/* First we implement our hash family */
	hashTbl::hash::hash(unsigned rangebits, const uint32_t* a,
					const uint64_t* alpha, const uint64_t* beta) {
		this->rangebits = rangebits;
		if (a) {
			for (size_t i = 0; i < aLen; i++) {
				this->a[i] = a[i] | 1; /* make sure it is odd. */
			}
		} else {
			for (size_t i = 0; i < aLen; i++) {
				this->a[i] = R32 | 1;
			}
		}
		this->alpha = ((alpha) ? *alpha : R64) | 1;
		this->beta = ((beta) ? *beta : R64);
		/* only keep the low order bits of beta: */
		this->beta &= ((uint64_t)-1)>>(64 - rangebits);
	}
	uint64_t hashTbl::hash::operator()(const string& x) const {
		assert(x.length() <= 4*aLen);
        string n = x;

        if(n.length()%8 !=0)
            for(size_t i = 0; i < (8 - (x.length()%8)); i++)
                n.push_back('\0');

        const uint32_t* D = reinterpret_cast<const uint32_t*>(n.c_str());
        uint64_t h_a = 0;
        uint64_t g_a_b = 0;
        for (size_t i = 0; i < n.length()/8; i++)
            h_a +=(uint64_t)(D[2*i] +this->a[2*i])*(D[2*i+1] + this->a[2*i+1]);

       return g_a_b =((h_a*(this->alpha) + this->beta)>>((uint64_t)(64-(this->rangebits))));

    }

	//constructors:
	hashTbl::hashTbl(unsigned nBits, const uint32_t* ha,const uint64_t* halpha, const uint64_t* hbeta) :nBits(nBits),h(nBits,ha,halpha,hbeta)
	{
		this->table = new list<val_type>[TLEN];
	}
	hashTbl::hashTbl(const hashTbl& H)
	{
		/* TODO: write this */
		/* NOTE: the underlying linked list class has a working
		 * assignment operator! */

        this->nBits = H.nBits;
        this->h = H.h;

        this->table = new list<val_type>[TLEN];

        for(size_t i = 0 ; i < TLEN ; i++)
            this->table[i] = H.table[i];


	}

	//destructor:
	hashTbl::~hashTbl()
	{
		delete[] this->table;
		//NOTE: this will call the destructor of each of the linked lists,
		//so there isn't anything else that we need to worry about.
	}

	//operators:
	hashTbl& hashTbl::operator=(const hashTbl& H)
	{
		/* TODO: write this */

        if( this == &H)
     		return *this;
            
        delete [] this->table;

        this->nBits = H.nBits;
        this->h = H.h;
        
        this->table = new list<val_type>[TLEN];

        for(size_t i = 0; i < TLEN; i++)
            this->table[i] = H.table[i];
 
         return *this;
	}

	ostream& operator<<(ostream& o, const hashTbl& H)
	{
		for (size_t i = 0; i < H.tableLength(); i++) {
			o << "[" << setfill('0') << setw(2) << i << "] |";
			for (list<val_type>::iterator j = H.table[i].begin();
					j != H.table[i].end(); j++) {
				o << *j << "|";
			}
			o << endl;
		}
		return o;
	}

	void hashTbl::insert(val_type x)
	{
/*
 *       list<val_type> L = this->table[h(x)];
 *       if (find(L.begin(),L.end(),x) != L.end()) { return;}
 *
 */
       if(!search(x))
       this->table[h(x)].push_front(x);
	}

	void hashTbl::remove(val_type x)
	{
        table[h(x)].remove(x);      
	}

	void hashTbl::clear()
	{
		for(size_t i=0; i<TLEN; i++)
			this->table[i].clear();
	}

	bool hashTbl::isEmpty() const
	{
		/* look for any non-empty list. */
		for(size_t i=0; i<TLEN; i++)
			if(!this->table[i].empty()) return false;
		return true;
	}

	bool hashTbl::search(val_type x) const
	{
        std::list<val_type>::iterator it;
        
        it = find(table[h(x)].begin(), table[h(x)].end(),x);
        if(it !=table[h(x)].end()) {return true;}

        else
        return false;

	}

	size_t hashTbl::countElements() const
	{
		/* TODO: write this */
        size_t counter = 0;

        for(size_t i = 0; i < TLEN; i++)
            for(list<val_type>::iterator it = this->table[i].begin(); it != this->table[i].end(); it++)
                counter++;

		return counter; //just so it compiles for now...
	}

	size_t hashTbl::tableLength() const
	{
		return TLEN;
	}

	size_t hashTbl::countCollisions() const
	{
		//just count the number of lists of length > 1
		size_t i,nCollisions=0;
		for(i=0; i<TLEN; i++)
			if(table[i].size() > 1)
				++nCollisions;
		return nCollisions;
	}

	size_t hashTbl::longestListLength() const
	{
		/* TODO: write this */
        size_t longest = 0;
        size_t length = 0;

        for(size_t i = 0; i < TLEN; i++){
            length = table[i].size();
            if(length > longest) longest = length;}

		return longest; //just so it compiles for now...
    }

}

