/****************************************************
implementation file for linked list class.
*****************************************************/

#include "list.h"
#include <stdlib.h> //for rand()


#include <iostream>
using namespace std;



namespace csc212
{
	/* listNode stuff: */
	listNode::listNode()
	{
		this->next = 0; //set the next pointer to be null
	}

	listNode::listNode(val_type x, listNode *pNext)
	{
		this->data = x;
		this->next = pNext;
	}


	/* linked list implementation: */
	list::list()
	{
		this->root = 0; //initialize the list to be empty
		//remember 0 == NULL, which is never memory you own.
	}

	list::list(const list &L)
	{
		//we need to make *this a copy of L
		listNode* pL, *p; //pL will traverse L, p will traverse *this
		pL = L.root;
		root = 0; //start out with the empty list
		if(L.root != 0)
		{
			this->root = new listNode(pL->data);
			pL = pL->next;
		}
		p = this->root;
		//loop invariant:

		/*pL is a pointer to the next node to be copied in L
		p is a pointer to the last node created in (*this)
		in english: pL goes through L and is is one step ahead of p,
		which is going through (*this)
		our loop should add a new node to *this, and then maintain the invariant*/

		while(pL != 0) //pL != NULL
		{
			p->next = new listNode(pL->data);
			p = p->next;
			pL = pL->next;
		}
	}

	list::~list()
	{
		this->clear(); //delete all nodes, deallocating memory
	}

	list& list::operator=(const list& L)
	{
		//idea: erase our contents, then copy L, similar to the copy constructor.
		//but first, check for self-assignment:
		if(this == &L) return *this;

		clear();
		listNode* pL, *p;
		pL = L.root;
		if(pL)
		{
			root = new listNode(pL->data);
			pL = pL->next;
		}
		p = root;
		while(pL)
		{
			p->next = new listNode(pL->data);
			p = p->next;
			pL = pL->next;
		}
		return *this;
	}

	bool operator==(const list& L1, const list& L2)
	{
		/* TODO: write this. */
		if(L1.length() != L2.length()) return false;
		
		else if(L1.length() == 0 && L2.length() == 0) return true; 
		
		else 
		{
			listNode * p1;
			listNode * p2;
			p1 = L1.root;
			p2 = L2.root;
			
			do
			{
				if(p1->data != p2->data) return false; 
				p1 = p1->next;
				p2 = p2->next;
			}
			
			while(p1 && p2);
			
			return true;
		}
	}
	
	bool operator !=(const list& L1, const list& L2)
	{
		return !(L1==L2);
	}

	void list::insert(val_type x)
	{
	
		listNode * newNode = new listNode;
		newNode->data = x;
		
		if(this->root == NULL)
		{
			this->root = newNode;
			return;
		}
		
		if(this->root->data >= newNode->data)
		{
			newNode->next = this->root;
			this->root = newNode;
			return;
		}
		
		
		else
		{
			listNode * guyBefore = 0;
			listNode * target = this->root;
			
			while(target->data < newNode->data && target->next != NULL)
			{
				guyBefore = target;
				target = target->next;

			}
			
			if(target->next == 0)
			{
				if(target->data > newNode->data)
				{
					newNode->next = target;
					guyBefore->next = newNode;
					guyBefore = NULL;
					target = NULL;
					return;
				}
				
				else
				{
					target->next = newNode;
					guyBefore = NULL;
					target = NULL;
					return;
				}

			}
			
			if(target != NULL)
			{
				newNode->next = target;
				guyBefore->next = newNode;
				guyBefore = NULL;
				target = NULL;
				return;
			}
		}

		
	}

	void list::remove(val_type x)
	{
		/* TODO: write this. */
		
		if(this->root->data > x) return;
		
		
		listNode * target = this->root;
		
		
		if(this->root->data == x) 
		{
			this->root = this->root->next;
			delete target;
			return;
		}
		

		listNode * guyBefore = 0;
	
		
		while(target->data < x &&!(target->next))
		{
			guyBefore = target;
			target = target->next;
		}
		
		if(target->data == x)
		{
			guyBefore->next = target->next;
			delete target;
			return;
		}
		
		else return;
	
	}

	bool list::isEmpty()
	{
		return (this->root == 0);
	}

	void list::clear()
	{
		//idea: repeatedly delete the root node...
		listNode* p;
		while((p = root)) //yes, I do mean "=", not "=="
		{
			root = p->next;
			delete p;
		}
	}

	ostream& operator<<(ostream& o, const list& L)
	{
		listNode* p;
		p = L.root;
		while(p)
		{
			o << p->data << " ";
			p = p->next;
		}
		return o;
	}

	bool list::search(val_type x) const
	{
		listNode* p;
		p = root;
		while(p && p->data != x) //again, short circuit evaluation is important...
			p = p->next;
		if(p) 
			return true;
		else 
			return false;
	}

	unsigned long list::length() const
	{
		/* TODO: write this. */
		if(this->root == NULL) return 0;
		
		else
		{
			listNode * position = this->root;
			unsigned long size = 1;
		
			while(position->next != NULL)
			{
				position = position->next;
				size++;
			}
			 //just so it compiles.  you of course need to do something different.
			return size;
		}
		
		
		
	}

	void list::merge(const list& L1, const list& L2)
	{

	//The first and second while loops will run iff exactly one of the lists are initially empty which will run through that lists length or O(n)
	//The thrid while loop will run if both lists are initially non empty up until the shorter one becomes empty about O(n)
	//The fourth and fifth while loops will run after the third to finish off the larger list which will be about O(n)
		
		// O(1) time
		listNode * p1 = L1.root;
		listNode * p2 = L2.root;
		listNode * p3;
		root = 0;

		
		// O(1) time
		if(L1.root == 0 && L2.root == 0)
		{
			return;
		}
		

		if(L1.root !=0 && L2.root == 0) 
		{
			this->root = new listNode(p1->data);
			p3 = this->root;
			
		    //O(n)
			while(p1->next != 0)
			{
				p1 = p1->next;
				p3->next = new listNode(p1->data);
				p3 = p3->next;
			}
			return;
		}
		
	
		if(L1.root ==0 && L2.root != 0) 
		{
			this->root = new listNode(p2->data);
			p3 = this->root;

		    //O(n)	
			while(p2->next != 0)
			{
				p2 = p2->next;
				p3->next = new listNode(p2->data);
				p3 = p3->next;
			}
			return;
		}	
			
		//O(1)
		if(p1->data > p2->data)
		{
			root = new listNode(p2->data);
			p3 = root;
			p2 = p2->next;
		}
		
		//O(1)
		else
		{
			root = new listNode(p1->data);
			p3 = root;
			p1 = p1->next;
		}
		
		//O(n)
		while(p1 != 0 && p2 !=0)
		{
			if(p1->data > p2->data)
			{
				p3->next = new listNode(p2->data);
				p3 = p3->next;
				p2 = p2->next;
			}
			else
			{
				p3->next = new listNode(p1->data);
				p3 = p3->next;
				p1 = p1->next;
			}
		}
		
		if(p1 == 0)
		{
			//O(n)
			while(p2 !=0)
			{
				p3->next = new listNode(p2->data);
				p3 = p3->next;
				p2 = p2->next;
			}
			return;
		}	
		

		else
		{
		    //O(n)
			while(p1 !=0)
			{
				p3->next = new listNode(p1->data);
				p3 = p3->next;
				p1 = p1->next;
			}
			return;			
		}
		

	}

	void list::randomFill(unsigned long n, unsigned long k)
	{
		//we want to fill the list with n random integers from 0..k-1
		this->clear(); //reset to the empty list
		unsigned long i;
		for(i=0; i<n; i++)
		{
			this->insert((val_type)(rand()%k));
		}
	}



	void list::intersection(const list &L1, const list& L2)
	{

	//Because I only have one loop which is a while loop which will only run as many times
	// as the smallest list, the intersection function runs in linear time O(n)
		
		// O(1) time
		listNode * p1 = L1.root;
		listNode * p2 = L2.root;
		listNode * p3;
		root = 0;
		p3 = this->root;


		//O(1)
		if(p1 == 0 || p2 == 0)
		{
			p1 = NULL;
			p2 = NULL;
			p3 = NULL;
			return;
		}
		
		//O(n) 
		while(p1 != 0 && p2 != 0)
		{
			if(p1->data == p2->data)
			{
				if(root == NULL)
				{
					root = new listNode(p1->data);
					p3 = root;
				}
				
				else 
				{
					p3->next = new listNode(p1->data);
					p3 = p3->next;
				}
				
				p2 = p2->next;
				p1 = p1->next;
				
				if(p1 == 0 || p2 == 0)
				{
					p1 = NULL;
					p2 = NULL;
					p3 = NULL;					
					return;
				}
			}
			
			else if (p1-> data > p2->data)
			{
				p2 = p2->next;
				if(p2 == 0)
				{
					p1 = NULL;
					p2 = NULL;
					p3 = NULL;					
					return;
				}
			}
			
			else
			{
				p1 = p1->next;
				if(p1 == 0)
				{
					p1 = NULL;
					p2 = NULL;
					p3 = NULL;					
					return;				
				
				} 
			}
		}

		p1 = NULL;
		p2 = NULL;
		p3 = NULL;		
		return;

	}
}
